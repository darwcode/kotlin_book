package co.com.darwinjimenez.myfirstkotlinapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.com.darwinjimenez.myfirstkotlinapp.R
import co.com.darwinjimenez.myfirstkotlinapp.domain.ForecastList

class ForecastListAdapter(val forecastList: ForecastList,
                          val itemClick : OnClickListener) : RecyclerView.Adapter<ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder{
        val view = LayoutInflater.from(parent.context) .inflate(R.layout.item_forecast, parent, false)
        return ViewHolder(view, itemClick)
    }



    override fun getItemCount(): Int = forecastList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindForecast(forecastList[position])
    }

}