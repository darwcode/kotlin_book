package co.com.darwinjimenez.myfirstkotlinapp.api

import android.util.Log
import java.net.URL

class Request(val url : String){


    fun run() : String{
        val forecastJsonStr = URL(url).readText()
        Log.d(javaClass.canonicalName.toString(),forecastJsonStr)
        return forecastJsonStr;

    }
}