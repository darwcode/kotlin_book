package co.com.darwinjimenez.myfirstkotlinapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.com.darwinjimenez.myfirstkotlinapp.domain.Forecast
import co.com.darwinjimenez.myfirstkotlinapp.domain.ForecastList
import co.com.darwinjimenez.myfirstkotlinapp.domain.RequestForecastCommand
import co.com.darwinjimenez.myfirstkotlinapp.ui.ForecastListAdapter
import co.com.darwinjimenez.myfirstkotlinapp.ui.OnClickListener
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val txtMessage = findViewById<TextView>(R.id.txt_message)
        txtMessage.text = "Hola Darwin"



        GlobalScope.async(Dispatchers.Main) {

            val result = GlobalScope.async(Dispatchers.IO){
                RequestForecastCommand("Maracaibo").execute();
            }.await()

            updateRecyclerView(result)
        }

    }




    @MainThread
    private fun updateRecyclerView(forecastList:ForecastList){
        val rvForecastList = findViewById<RecyclerView>(R.id.rv_forecast_list)
        rvForecastList.layoutManager = LinearLayoutManager(this)
        rvForecastList.adapter = ForecastListAdapter(
            forecastList,
            object : OnClickListener {

                override fun invoke(forecast: Forecast) {
                    toast(forecast.date)
                }
            })
    }



    fun Context.toast(message: CharSequence, duration: Int = Toast.LENGTH_LONG){
        Toast.makeText(this,message,duration).show();
    }


}
