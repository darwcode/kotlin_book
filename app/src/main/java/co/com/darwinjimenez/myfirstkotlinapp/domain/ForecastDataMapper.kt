package co.com.darwinjimenez.myfirstkotlinapp.domain

import co.com.darwinjimenez.myfirstkotlinapp.api.Main
import co.com.darwinjimenez.myfirstkotlinapp.api.Weather
import co.com.darwinjimenez.myfirstkotlinapp.api.WheatherResponse
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList

class ForecastDataMapper {


    public fun convertResponseToForecastList(response: WheatherResponse) : ForecastList{
        return ForecastList(city = response.name,
            country =  response.sys.country,
            dailyForecast = convertResponseToListOfForecast(response))
    }

    private fun convertResponseToListOfForecast(response : WheatherResponse) : List<Forecast>{
        val forecastList :  ArrayList<Forecast> = ArrayList()
        for (i in response.weather.indices){
            forecastList.add(convertResponseToDomainItem(response.weather[i],response.main,response.dt));
        }
        return forecastList;
    }



    private fun convertResponseToDomainItem(weather : Weather,main : Main, dt : Long) : Forecast{
        return Forecast(convertDate(dt), weather.description, main.temp_max.toInt(),
            main.temp_min.toInt(), generateIcon(weather.icon))
    }

    private fun convertDate(date: Long): String {
        val df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault())
        return df.format(date *1000)
    }

    private fun generateIcon (iconCode: String) : String{
        return "https://openweathermap.org/img/w/$iconCode.png"
    }
}