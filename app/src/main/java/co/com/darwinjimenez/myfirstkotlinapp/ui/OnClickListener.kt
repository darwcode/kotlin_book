package co.com.darwinjimenez.myfirstkotlinapp.ui

import co.com.darwinjimenez.myfirstkotlinapp.domain.Forecast

interface OnClickListener {
    operator fun invoke(forecast: Forecast);
}