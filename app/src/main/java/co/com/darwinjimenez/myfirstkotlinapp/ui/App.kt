package co.com.darwinjimenez.myfirstkotlinapp.ui

import android.app.Application
import android.database.sqlite.SQLiteOpenHelper

class App: Application {

    companion object{
         var instance : App by DelegateExt.notNullSingleValue()
    }


    override fun onCreate() {
        super.onCreate()
        instance = this;
    }

    fun initConfig(){
        val config = Configuration(mapOf("width" to 1080, "height" to 720, "dp" to 240, "deviceName" to "myDevice"))
    }
}