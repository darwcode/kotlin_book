package co.com.darwinjimenez.myfirstkotlinapp.api

import com.google.gson.Gson

class ForecastRequest(val cityName: String) {
    companion object{
        private val APP_ID = "ef063f6d6e6ff62ccbc1127167b1f1b0"
        private val UNIT = "metric"
        private val URL = "https://api.openweathermap.org/data/2.5/weather"
        private val COMPLETE_URL = "$URL?appid=$APP_ID&units=$UNIT&q="
    }

    fun execute() : WheatherResponse {
        val request = Request(COMPLETE_URL + cityName);
        val json = request.run();
        return Gson().fromJson(json, WheatherResponse::class.java)
    }
}