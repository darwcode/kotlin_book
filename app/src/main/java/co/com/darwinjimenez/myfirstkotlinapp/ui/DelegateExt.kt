package co.com.darwinjimenez.myfirstkotlinapp.ui

object DelegateExt {
    fun <T> notNullSingleValue() = NotNullSingleValueVar<T>()
}