package co.com.darwinjimenez.myfirstkotlinapp.ui

class Configuration(map: Map<String, Any?>) {
    val width: Int by map
    val height: Int by map
    val dp: Int by map
    val deviceName: String by map
}