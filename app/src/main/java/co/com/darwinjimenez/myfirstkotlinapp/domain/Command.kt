package co.com.darwinjimenez.myfirstkotlinapp.domain

interface Command<out T> {
    fun execute(): T
}