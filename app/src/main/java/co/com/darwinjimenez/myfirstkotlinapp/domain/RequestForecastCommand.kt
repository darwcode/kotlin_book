package co.com.darwinjimenez.myfirstkotlinapp.domain

import co.com.darwinjimenez.myfirstkotlinapp.api.ForecastRequest

class RequestForecastCommand(val cityName: String) :
        Command<ForecastList> {
    override fun execute(): ForecastList {
        val wheaterResult = ForecastRequest(cityName).execute();
        return ForecastDataMapper().convertResponseToForecastList(wheaterResult)
    }
}