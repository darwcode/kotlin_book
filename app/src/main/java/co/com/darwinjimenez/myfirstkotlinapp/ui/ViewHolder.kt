package co.com.darwinjimenez.myfirstkotlinapp.ui

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.com.darwinjimenez.myfirstkotlinapp.R
import co.com.darwinjimenez.myfirstkotlinapp.domain.Forecast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_forecast.view.*
import org.jetbrains.anko.find
import kotlin.math.max

class ViewHolder(view: View, val itemClick: OnClickListener) : RecyclerView.ViewHolder(view) {


    fun bindForecast(forecast: Forecast){
        with(forecast){
            val picasso = Picasso.get()

            picasso.load(iconUrl).into(itemView.img_icon)
            itemView.txt_city.text = date
            itemView.txt_description.text = description
            itemView.txt_max_temperature.text = high.toString()
            itemView.txt_min_temperature.text = low.toString()
            itemView.setOnClickListener { itemClick(this) }
        }
    }
}