package co.com.darwinjimenez.myfirstkotlinapp

import co.com.darwinjimenez.myfirstkotlinapp.api.*
import co.com.darwinjimenez.myfirstkotlinapp.domain.ForecastDataMapper
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

@RunWith(MockitoJUnitRunner::class)
class ExampleUnitTest {




    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun isMapperWorking(){
        val mapper:ForecastDataMapper = ForecastDataMapper()
        val coord = Coord(26.0,25.0);

        val main = Main(0.0,0.0,27.6,28.6,0,0,0,0);
        val weather = Weather(0,"Maracaibo","Clear","Icono");
        val sys = Sys("VE",0,0);
        val wind = Wind(0.0,0);
        val clouds = Clouds(3);

        val response : WheatherResponse = WheatherResponse(coord,listOf(weather)
            ,"FAke",main,wind,clouds,1583425497,sys,
            0,123,"Maracaibo",303)

        val result = mapper.convertResponseToForecastList(response)

        assertEquals(result.dailyForecast[0].description,"Clear")

    }
}
